package test;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.*;

public class ExchangeClient{

    private FhirContext ctx;
    private IGenericClient client;

    public ExchangeClient(FhirContext ctx,String fhirServerUrl, String bearer){
        //IBaseResource a;
        this.ctx = ctx;
        this.client = this.ctx.newRestfulGenericClient(fhirServerUrl);
        this.client.registerInterceptor(new RequestHeaderInterceptor(bearer));
    }


    public MethodOutcome sendPatient(Patient p){
        MethodOutcome outcome = sendPutRequest(p);
        return outcome;
    }

    private MethodOutcome sendGetRequest(){
        
        return null;
    }
    private MethodOutcome sendPostRequest(){

        return null;
    }
    private MethodOutcome sendPutRequest(IBaseResource resource){
        MethodOutcome outcome = client.update().resource(resource).execute();
        return outcome;
    }
}
