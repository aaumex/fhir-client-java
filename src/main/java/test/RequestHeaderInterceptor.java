package test;

import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Interceptor;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.rest.client.api.IHttpRequest;
import ca.uhn.fhir.rest.client.api.IRestfulClient;

//import org.springframework.core.env.ConfigurableEnvironment;

@Interceptor
public class RequestHeaderInterceptor {

    //private ConfigurableEnvironment configurableEnvironment;
    private String bearer;
    public RequestHeaderInterceptor(String bearer){//ConfigurableEnvironment configurableEnvironment) {
        //this.configurableEnvironment = configurableEnvironment;
        this.bearer = "Bearer " + bearer.trim();
    }

    @Hook(Pointcut.CLIENT_REQUEST)
    public void clientRequest(IHttpRequest theRequest, IRestfulClient theClient) {
        theRequest.addHeader("Authorization", this.bearer);
    }


}

       