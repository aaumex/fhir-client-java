package test;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.Patient;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.*;

public class PatientResourceComposer{

    private FhirContext ctx;
    public PatientResourceComposer(FhirContext ctx){
        this.ctx = ctx;
    }


    public Patient composePatient(String cid){

        String file_content="";
        try (Scanner file_scanner = new Scanner(new File("/home/hapi/hapi/gbdi-fhir-jpaserver/test_data/mockup_resource_all_pass/Patient.json")))
        {
            file_content = file_scanner.useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        EncodingEnum encoding = EncodingEnum.detectEncodingNoDefault(file_content);
        IParser parser = encoding.newParser(ctx);

        IBaseResource parsedResource = parser.parseResource(file_content);

        return (Patient) parsedResource;
                
    }


}
