package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Setter;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HIEClient {
    
    // @Setter
    // private static ObjectMapper mapper;

    // @Setter
    private static OkHttpClient client;
    public HIEClient(){
        client = getUnsafeOkHttpClient();
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static String hashCidSetToJson(HashSet<String> hashedCidSet,String mdpID){
        //form json string 
        String json = "{ "+"\"mdp9_id\": \""+mdpID+"\", "+"\"hash_cid\": [";
        for (String hcid : hashedCidSet){
            json+="\"" + hcid+"\"" + ",";
        }
        return json.substring(0,json.length()-1) + " ]}";
    }

    public String GetCidList(HashSet<String> hashedCidSet){

        //check alpha numeric for id if not then leave as it is. If not, add to to get one.

        try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            String mdpID = prop.getProperty("mdpID_value");
            //String json = hashCidSetToJson(hashedCidSet,mdpID);
            //String hie_base_url = prop.getProperty("hie_base_url_path"); 
        
            //RequestBody body = RequestBody.create(JSON, json);

            String url = prop.getProperty("hie_base_url_path")+prop.getProperty("hie_cid_pair_endpoint").replace("{mdp9_ID}",mdpID);
            String skip = prop.getProperty("skip");
            String limit = prop.getProperty("limit");
            String lastupdated = prop.getProperty("lastupdated");
            url = url + "?skip="+skip+"&limit="+limit+"&lastupdated="+lastupdated;

            Request request = new Request.Builder()
            .url(url)
            .addHeader(
            prop.getProperty("hie_header_key"),
            prop.getProperty("hie_header_value"))
            .get()
            .build();


            try (Response response = client.newCall(request).execute()) {
                String strResponse = response.body().string();
                return strResponse;
            }
            catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (IOException ex) {
        ex.printStackTrace();
        return null;
        }
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
          // Create a trust manager that does not validate certificate chains
          final X509TrustManager [] trustAllCerts = new X509TrustManager[] {
              new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }
    
                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }
    
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                  return new java.security.cert.X509Certificate[]{};
                }
              }
          };
    
          // Install the all-trusting trust manager
          final SSLContext sslContext = SSLContext.getInstance("SSL");
          sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
          // Create an ssl socket factory with our all-trusting manager
          final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
    

          OkHttpClient.Builder builder = new OkHttpClient.Builder();

          builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0]);
          builder.hostnameVerifier(new HostnameVerifier() {
              @Override
              public boolean verify(String hostname, SSLSession session) {
                  return true;
              }
          });
          OkHttpClient okHttpClient = builder.build();
    
          return okHttpClient;
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
    }
    public static void main(String[] args){
        HIEClient cli = new HIEClient();
        System.out.println(cli.GetCidList(null));
    }
}
