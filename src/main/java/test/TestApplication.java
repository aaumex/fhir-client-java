package test;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.hl7.fhir.r4.model.Patient;

public class TestApplication {

   /**
    * This is the Java main method, which gets executed
    */
   public static void main(String[] args) {

      // // Create a context
      // FhirContext ctx = FhirContext.forR4();

      // // Create a client
      // IGenericClient client = ctx.newRestfulGenericClient("https://hapi.fhir.org/baseR4");

      // // Read a patient with the given ID
      // Patient patient = client.read().resource(Patient.class).withId("example").execute();

      // // Print the output
      // String string = ctx.newJsonParser().setPrettyPrint(true).encodeResourceToString(patient);
      // System.out.println(string);

      String url = "http://localhost:8080/fhir";
      PatientResourceComposer prc = new PatientResourceComposer(FhirContext.forR4());
      Patient p = prc.composePatient("1234567893214");
      IParser parser = FhirContext.forR4().newJsonParser();
      System.out.println(parser.encodeResourceToString(p));
      
      String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwZXJtaXNzaW9ucyI6WyJOb3RSZXF1aXJlRGVJZGVudGlmeSIsIk1hbmFnZUNyZWRlbnRpYWwiLCJBbGxvd0FsbFJ1bGVCdWlsZGVyIiwiTm90UmVxdWlyZUNvbnNlbnQiXSwiZXhwIjoxNjE1NjI3MzE1LCJ1c2VybmFtZSI6IkFkbWluIn0.7WlD0Uk1dMVnvgP0eIQXqkgrW7PE1sXRLqWNbkJ5I_E";

      ExchangeClient cli = new ExchangeClient(FhirContext.forR4(), url,token);
      MethodOutcome outcome = cli.sendPatient(p);
      System.out.println(parser.encodeResourceToString(outcome.getResource()));
   }

}
